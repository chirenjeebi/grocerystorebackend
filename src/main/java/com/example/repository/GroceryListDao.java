package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.model.GroceryList;

@Repository
public interface GroceryListDao extends JpaRepository<GroceryList, Integer>{
	
	public List<GroceryList> findAll();
	public GroceryList findBygroceryName(String groceryName);
//	public List<GroceryList> findListById(String itemName);


}
