package com.example.controller;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.GroceryItem;
import com.example.model.GroceryList;
import com.example.service.GroceryItemService;
import com.example.service.GroceryListService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;


@RestController
@RequestMapping(value="/api/grocery")
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Autowired))
@CrossOrigin(origins = "http://localhost:4200")
public class GroceryListController {
	
	private GroceryListService groceryListService;
	
	private GroceryItemService groceryItemServ; 
		
	@PostMapping("/list")
	public ResponseEntity<String> insertGrocery(@RequestBody LinkedHashMap<String, String> groceryMap){
		String groceryName = groceryMap.get("groceryName");
		double price =Double.parseDouble(groceryMap.get("price"));
		int itemId =  Integer.parseInt(groceryMap.get("itemId"));
		
		GroceryItem groItemObj = groceryItemServ.getById(itemId);
		
		GroceryList groceryList = new GroceryList(groceryName, price , groItemObj);
		
		groceryListService.insertGroceryList(groceryList);
		
		return new ResponseEntity<>("Resource was created", HttpStatus.CREATED);
	}
	
	@GetMapping("/{name}")
	public ResponseEntity<GroceryList> getGroceryByName(@PathVariable("groceryName") String name){
		if(groceryListService.getGroceryByName(name)== null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(groceryListService.getGroceryByName(name), HttpStatus.OK);
	}
	
//	@GetMapping("/list")
//	public ResponseEntity<GroceryList> getAllGroceryList(@PathVariable String name){
//		List<GroceryList> groList = groceryListService.getAllList();
//		return new ResponseEntity<>(groList, HttpStatus.OK);
//		
//	}
	
//	@GetMapping("/view")
//	public ResponseEntity<List<GroceryList>> viewTransaction(@RequestParam("itemId") String itemId) {
//		List<GroceryList> gList = groceryListService.getItemListById(itemId);
//		if(gList== null) {
//			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
//		}
//
//		return new ResponseEntity<>(gList, HttpStatus.OK);
//	}

}
