package com.example.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.GroceryItem;
import com.example.service.GroceryItemService;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@RestController
@RequestMapping(value="/api/grocery")
@NoArgsConstructor
@AllArgsConstructor(onConstructor = @__(@Autowired))
@CrossOrigin(origins = "http://localhost:4200")
public class GroceryItemController {
	
	private GroceryItemService groceryItemServ;
	
	
@PostMapping("/item")
public ResponseEntity<String> insertGroceryItem(@RequestBody LinkedHashMap<String, String> itemMap){
	String itemName = itemMap.get("itemName");

	GroceryItem groceryItem = new GroceryItem(itemName);
	
	groceryItemServ.insertGroceryItem(groceryItem);
	
	return new ResponseEntity<>("Grocery Items created successfully", HttpStatus.CREATED);
}

}
