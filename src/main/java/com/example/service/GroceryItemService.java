package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.model.GroceryItem;
import com.example.model.GroceryList;
import com.example.repository.GroceryItemDao;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class GroceryItemService {
	
	private GroceryItemDao groceryItemDao;
	
	public GroceryItem getGroceryItemByName(String itemName) {	
		return groceryItemDao.findByitemName(itemName);
	}
	
	public GroceryItem getById(int itemId) {	
		return groceryItemDao.findByitemId(itemId);
	}
	
	
	public List<GroceryItem> getAllItem(){
		return groceryItemDao.findAll();
	}
	
	public GroceryItem insertGroceryItem(GroceryItem groceryItem) {
		groceryItemDao.save(groceryItem);
		return groceryItem;
	}



}
