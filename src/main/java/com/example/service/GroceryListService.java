package com.example.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.model.GroceryList;
import com.example.repository.GroceryListDao;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor
public class GroceryListService {
	
	
	private GroceryListDao groceryListDao;
	
	public GroceryList getGroceryByName(String groceryName) {	
		return groceryListDao.findBygroceryName(groceryName);
	}
	
	
	public List<GroceryList> getAllList(){
		return groceryListDao.findAll();
	}
	
	public GroceryList insertGroceryList(GroceryList grocery) {
		groceryListDao.save(grocery);
		return grocery;
	}


//	public List<GroceryList> getItemListById(String itemName) {
//		return groceryListDao.findListById(itemName);
//	}
	

}
