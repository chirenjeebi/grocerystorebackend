package com.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name ="grocery_list")
public class GroceryList {
	
	@Id
	@Column(name="list_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int groceryId;
	
	@Column(name="grocery_name")
	private String groceryName;
	
	@Column(name="price")
	private double price;
	
	@ManyToOne(fetch=FetchType.EAGER )
	@JoinColumn(name="groItem_fk")
	private GroceryItem groItem;

	public GroceryList(String groceryName, double price, GroceryItem groItem) {
		super();
		this.groceryName = groceryName;
		this.price = price;
		this.groItem = groItem;
	}

	@Override
	public String toString() {
		return "GroceryList [groceryId=" + groceryId + ", groceryName=" + groceryName + ", price=" + price
				+ ", groItem=" + groItem + "]";
	}
	
}
